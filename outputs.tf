output "ids" {
  value       = tolist(data.aws_network_interfaces.nis.ids)
  description = "List of the network interfaces IDs matching the filters"
}