data "aws_arn" "lb" {
  arn = var.lb_arn
}

locals {
  lb_name = substr(data.aws_arn.lb.resource, length("loadbalancer/"), -1)
  //removing description filter, it is set by the module
  additional_filters = [
    for filter in var.additional_filters :
    filter
    if filter.name != "description"
  ]
}

data "aws_network_interfaces" "nis" {
  filter {
    name = "description"
    values = [
      "ELB ${local.lb_name}"
    ]
  }

  dynamic "filter" {
    for_each = local.additional_filters

    content {
      name   = filter.name
      values = filter.values
    }
  }
}