package main

# Accessors

variable_lb_arn := variable("lb_arn")

variable_additional_filters := variable("additional_filters")

output_ids := output("ids")

data_nis := data("aws_network_interfaces", "nis")

local_lb_name  := local("lb_name")

local_additional_filters := local("additional_filters")

# Rules

deny[msg] {
  count(variable_lb_arn) != 1
  msg = "Define the lb_arn variable"
}

deny[msg] {
  count(variable_additional_filters) != 1
  msg = "Define the additional_filters variable"
}

deny[msg] {
  count(output_ids) != 1
  msg = "Define the ids output"
}

deny[msg] {
  count(data_nis) != 1
  msg = "Define the network interfaces data source"
}

deny[msg] {
  count(local_lb_name) != 1
  msg = "Define the local to calculate the lb_name"
}

deny[msg] {
  count(local_additional_filters) != 1
  msg = "Define the local to sanitize the additional_filters"
}

deny[msg] {
  count([value | value := data_nis[_].filter.values[0]; value == "ELB ${local.lb_name}"]) != 1
  msg = "The network interfaces data source must use the lb_name local as value"
}

deny[msg] {
  count([value | value := data_nis[_].dynamic.filter.for_each; value == "${local.additional_filters}"]) != 1
  msg = "The network interfaces data source must iterate its filters on the additional_filters local"
}

deny[msg] {
  count([value | value := output_ids[_].value; value == "${tolist(data.aws_network_interfaces.nis.ids)}"]) != 1
  msg = "The ids output must use the network interfaces data source for its value"
}