# Get Network Interfaces IDS from Load Balancer
This module provides a way to retrieve the network interfaces IDS associated with a LB.    
It uses Terraform >= 0.12 for structure.
